/**
 * CLASE: Profesor.java
 * OBJETIVO: Clase que describe una SUBCLASE Profesor con carrera, semestre y promedio
 * ASIGNATURA: 
 * @version 1.0 24/07/2005
 * @author William Mendoza Rodrguez.
 */

public class Profesor extends Persona {
    
    // Atributos privados de la clase Profesor
    
    public String facultad;
    public String nacionalidad;
    public Asignatura materia;
        
    /** 
     * Constructor del objeto Profesor.
     *
     * @param nom      nombre del Profesor
     * @param num      numeroId del Profesor
     */
    public Profesor (String nom, int num) {
	super (nom, num); 
	facultad = "Por definir";
    }
    
    /** 
     * Constructor del objeto Profesor.
     *
     * @param nom      nombre del Profesor
     * @param num      numeroId del Profesor
     * @param car      facultad del Profesor
     */
    public Profesor (String nom, int num, String fac) {
	super (nom, num); 
	facultad = fac;
    }
    
    /** 
     * Constructor del objeto Profesor.
     *
     * @param nombre      nombre del Profesor
     * @param numeroId    numero de identidad del Profesor 
     * @param anio         anio de nacimiento del Profesor
     * @param mes         mes de nacimiento del Profesor
     * @param dia         dia de nacimiento del Profesor
     * @param fac      facultad del Profesor
     */
    public Profesor (String nombre, int numeroId, int anio, int mes, int dia) {
    	
    	
	super (nombre, numeroId, anio, mes, dia);
	
	
	
		this.facultad = "Por definir";
    }
    
    /** 
     * Constructor del objeto Profesor.
     *
     * @param nombre      nombre del Profesor
     * @param numeroId    numero de identidad del Profesor 
     * @param anio         anio de nacimiento del Profesor
     * @param mes         mes de nacimiento del Profesor
     * @param dia         dia de nacimiento del Profesor
     */
    public Profesor (String nombre, int numeroId, int anio, int mes, int dia, String fac) {
		super (nombre, numeroId, anio, mes, dia);
		facultad = fac;
    }
    
    /** 
     * Analizador para la facultad del Profesor.
     *
     * @return facultad del Profesor
     */
    public String getfacultad() { 
		return facultad; 
	
    }
    
    /** 
     * Modificador, Asigna facultad de este Profesor.
     *
     * @param f   facultad de este Profesor
     */
    public void setfacultad (String f) { 
	facultad = f; 
    }

 /** 
     * Analizador para la nacionalidad del Profesor.
     *
     * @return nacionalidad del Profesor
     */
    public String getNacionalidad() { 
	return nacionalidad; 
    }
    
    /** 
     * Modificador, Asigna nacionalidad de este Profesor.
     *
     * @param n   nacionalidad de este Profesor
     */
    public void setNacionalidad (String n) { 
	nacionalidad = n; 
    }

    /** 
     * Retorna Los datos del carro de la persona como string.
     *
     * @return datCar de esta persona
     */
       
       public String getDatosAsignatura() {
       	String datAsig ="";
       	
       	datAsig += materia.getDatosAsignatura();
       	
       	return datAsig;
       }
       
    /** 
     * Retorna la informacion de este Profesor como un String
     *
     * @return la informacion de este Profesor
     */
    public String toString() { 
	String s;
	s  = "Nombre    : " + getNombre() + "\n";
	s += "facultad   : " + getfacultad () + "\n";
	s += "Numero Identificacion: " + getNumeroId() + "\n";
	s += "Fecha nacimiento     : " + getFechaNacimiento() + "\n";
	s += "Nacionalidad : " + getNacionalidad() + "\n"; 
	s += "Vehiculo : \n";
	s += coche.getDatosCarro();
	s += "Asignaturas : \n";
	s += getDatosAsignatura();
	return s;
    }
    
}

