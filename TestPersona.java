/**
 * CLASE: TestPersona.java
 * OBJETIVO: Prueba de la SUPERCLASE Persona y sus SUBCLASES (Estudiante y
Profesor)
 * ASIGNATURA: 
 * @version 1.0 24/07/2005
 * @author William Mendoza Rodrguez.
 */

public class TestPersona {
    

/*=========================================================================
      DEFINICION E IMPLEMENTACION DE LOS METODOS ANALIZADORES Y
MODIFICADORES

=========================================================================*/
    
    /** 
     * Muestra una cadena en pantalla.
     * PRE: TRUE
     * @param s El string a mostrar
     */
    
    public static void show(String s) { 
	System.out.println(s);
    }
    
    /**
     * Un test de la clase. Retorna un string.
     * PRE: TRUE 
     * POS: Crea dos objetos uno de tipo persona (superclase) y otro al de
su subclase; este apunta a su subclase, VEA LO QUE SUCEDE
     * @return String con el texto de la Persona
     */
    public static String test() {
	String s="";
	
	Carro c1 = new Carro("Corsa 5p", "BKH-%$#", "Blanco",10000000.0);
	Carro c2 = new Carro("Renault 12 Break", "FBD-697", "Blanco",7000000.0);
	
	
	Persona p1, p2;
	
	Estudiante p3;
	
	Profesor p4;
	
	// Se crean dos objetos de tipo Persona, despues de los cambios se crean dos de tipo estudiante y uno de persona
	
	p1 = new Persona("Jose", 10, 1970, 10, 31);
	
	
	
	p2 = new Estudiante("William", 79909888, "Sistemas");
	p3 = (Estudiante) p2;  //como no puedo hacer el casteo, cambio p3 por p2!!!.

	p4 = new Profesor("Ivan", 177, 1977, 07, 25, "Sistemas");
	
	
	p3.setCarrera("Sistemas"); //Aqui cambio la carrera tanto para p2, como para p3, ya que ambos me apuntan al mismo estudiante.
	p3.semestre = /*7;*/ p3.getSemestre(); 
	p3.incrementarSemestre();
	p3.setPromedio(3.5f);

	//p4.setFacultad("Sistemas"); 

	p4.setNacionalidad("Mexicano");
	p3.setFechaNacimiento (1977, 11, 8);
	
	p3.setCarro(c2);
	p4.setCarro(c1);
	
	p4.materia = new Asignatura("POB", "Programacion Orientada a Objetos", 3, 2);
	//Asignatura (String cod, String nom, int cre, int num)
	
	s += "Persona 1: " + p1.toString() + "\n";
	s += "Persona 2: " + p2.toString() + "\n";
	s += "Persona 3: " + p3.toString() + "\n";
	s += "Persona 4: " + p4.toString() + "\n";
	s += "\n";
	
	return s;
    }
    

/*=========================================================================
      PROGRAMA PRINCIPAL 

=========================================================================*/
    /**
     * Metodo main para el test.
     */
    
    public static void main(String argv[]) {
	show(test());
    }
    
}


