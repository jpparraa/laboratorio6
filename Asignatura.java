public class Asignatura{
	private String codigoCurso;
	private String nombreCurso;
	private int creditos;
	private int numeroGrupos;

	public Asignatura(){
		this.codigoCurso = "";
		this.nombreCurso = "";
		this.creditos = 0;
		this.numeroGrupos = 0;
	}

	public Asignatura(String codigoCurso, String nombreCurso, int creditos, int numeroGrupos){
		this.codigoCurso = codigoCurso;
		this.nombreCurso = nombreCurso;
		this.creditos = creditos;
		this.numeroGrupos = numeroGrupos;
	}

	public String getCodigoCurso(){
		return this.codigoCurso;
	}

	public String getNombreCurso(){
		return this.nombreCurso;
	}

	public int getCreditos(){
		return this.creditos;
	}

	public int getNumeroGrupos(){
		return this.numeroGrupos;
	}

	public void setCodigoCurso(String cod){
		this.codigoCurso = cod;
	}

	public void setNombreCurso(String nombreCurso){
		this.nombreCurso = nombreCurso;
	}

	public void setCreditos(int creditos){
		this.creditos = creditos;
	}

	public void setNumeroGrupos(int numeroGrupos){
		this.numeroGrupos = numeroGrupos;
	}

	public String getDatosAsignatura(){
		return  "Código: " +codigoCurso+'\n'+
			"Nombre: " +nombreCurso+'\n'+
			"Créditos: " +creditos+'\n'+
			"Número de grupos: " +numeroGrupos+'\n';
	}
}
