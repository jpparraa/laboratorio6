/**
 * CLASE: TestCarro.java
 * OBJETIVO: Prueba de la Clase Carro
 * ASIGNATURA: 
 * @version 1.0 24/07/2005
 * @author William Mendoza Rodrguez.
 */

public class TestCarro {
    
    /*=========================================================================
      DEFINICION E IMPLEMENTACION DE LOS METODOS ANALIZADORES Y MODIFICADORES
      =========================================================================*/
    
    /** 
     * Muestra una cadena en pantalla.
     * PRE: TRUE
     * @param s El string a mostrar
     */
    
    public static void show(String s) { 
	System.out.println(s);
    }
    
    /**
     * Un test de la clase. Retorna un string.
     * PRE: TRUE 
     * POS: Crea un carro, y modifica sus datos
     * @return String con el texto del Carro
     */
    public static String test() {
	String s="";
	Carro c;
	
	
	c = new Carro("Corsa 5p", "BKH-%$#", "Blanco",1000000.0);
	
	s += c.getDatosCarro() + "\n";	
	// Cambiamos los datos del objeto conectado a c
	
	s += "... despues de los cambios\n";
	s += c.getDatosCarro() + "\n";
	
	return s;
    }
    
    /*=========================================================================
      PROGRAMA PRINCIPAL 
      =========================================================================*/
    /**
     * Metodo main para el test.
     */
    
    public static void main(String argv[]) {
	show(test());
    }
    
}
