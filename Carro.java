public class Carro{
	private String modelo;
	private String placa;
	private String color;
	private double precio;

	public Carro(){
		this.modelo = "";
		this.placa = "";
		this.color = "";
		this.precio = 0.0;
	}

	public Carro(String modelo, String placa, String color, double precio){
		this.modelo = modelo;
		this.placa = placa;
		this.color = color;
		this.precio = precio;
	}

	public String getModelo(){
		return this.modelo;
	}

	public String getPlaca(){
		return this.placa;
	}

	public String getColor(){
		return this.color;
	}

	public double getPrecio(){
		return this.precio;
	}

	public void setModelo(String modelo){
		this.modelo = modelo;
	}

	public void setPlaca(String placa){
		this.placa = placa;
	}

	public void setColor(String color){
		this.color = color;
	}

	public void setPrecio(double precio){
		this.precio = precio;
	}

	public String getDatosCarro(){
		return  "Modelo: " +getModelo()+ '\n' +
			"Placa:  " +getPlaca()+ '\n' +
			"Color:  " +getColor()+ '\n' +
			"Precio: " +getPrecio()+ '\n';
	}
}
