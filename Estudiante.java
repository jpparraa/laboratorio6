/**
 * CLASE: Estudiante.java
 * OBJETIVO: Clase que describe una SUBCLASE Estudiante con carrera, semestre y promedio
 * ASIGNATURA: 
 * @version 1.0 24/07/2005
 * @author  William Mendoza Rodrguez.
 */

public class Estudiante extends Persona {
    
    // Atributos privados de la clase Estudiante
    
    public String carrera;
    public int semestre = 1;
    public float promedio = 0.0f;
    
    
    /** 
     * Constructor del objeto Estudiante.
     *
     * @param nom      nombre del Estudiante
     * @param num      numeroId del Estudiante
     */
    
    
    
    public Estudiante (String nom, int num) {
    	
    	
	super (nom, num); 
	
	
	carrera = "Por definir";
    }
    
    /** 
     * Constructor del objeto Estudiante.
     *
     * @param nom      nombre del Estudiante
     * @param num      numeroId del Estudiante
     * @param car      carrera del Estudiante
     */
    
    public Estudiante (String nom, int num, String car) {
		super (nom, num); 
		carrera = car;
    }
    
    /** 
     * Constructor del objeto Estudiante.
     *
     * @param nombre      nombre del Estudiante
     * @param numeroId    numero de identidad del Estudiante 
     * @param anio         anio de nacimiento del Estudiante
     * @param mes         mes de nacimiento del Estudiante
     * @param dia         dia de nacimiento del Estudiante
     * @param car      carrera del Estudiante
     */
    public Estudiante (String nombre, int numeroId, int anio, int mes, int dia) {
		super (nombre, numeroId, anio, mes, dia);
	carrera = "Por definir";
    }
    
    /** 
     * Constructor del objeto Estudiante.
     *
     * @param nombre      nombre del Estudiante
     * @param numeroId    numero de identidad del Estudiante 
     * @param anio         anio de nacimiento del Estudiante
     * @param mes         mes de nacimiento del Estudiante
     * @param dia         dia de nacimiento del Estudiante
     */
    public Estudiante (String nombre, int numeroId, int anio, int mes, int dia, String car) {
	super (nombre, numeroId, anio, mes, dia);
	carrera = car;
    }
    
    /** 
     * Analizador para la carrera del Estudiante.
     *
     * @return carrera del Estudiante
     */
    public String getCarrera() { 
	return carrera; 
    }
    
    /** 
     * Modificador, Asigna carrera de este Estudiante.
     *
     * @param c   Carrera de este Estudiante
     */
    public void setCarrera (String c) { 
	carrera = c; 
    }
    
    /** 
     * Analizador para semestre del Estudiante.
     *
     * @return semestre del Estudiante
     */
    public int getSemestre () {
	return semestre; 
    }
    
   
    /** 
     * Incrementa el semestre de un Estudiante.
     */
    public void incrementarSemestre () { 
	semestre++; 
    }
    
    /** 
     * Analizador para promedio del Estudiante.
     *
     * @return promedio del Estudiante
     */
    public float getPromedio () {
	return promedio; 
    }
    
    /** 
     * Actualiza el promedio de un Estudiante.
     *
     * @param prom Nuevo promedio del estudiante
     */
    public void setPromedio (float prom) { 
	promedio = prom; 
    }
    
       
    
    /** 
     * Retorna la informacion de este Estudiante como un String
     *
     * @return la informacion de este Estudiante
     */
    public String toString() { 
	String s;
		
	s  = "Nombre    : " + getNombre() + "\n";
	s += "Carrera   : " + getCarrera () + "\n";
	s += "Semestre  : " + getSemestre () + "\n";
	s += "Promedio  : " + getPromedio () + "\n";
	s += "Numero Identificacion: " + getNumeroId() + "\n";
	s += "Fecha nacimiento     : " + getFechaNacimiento() + "\n";

	s += "Vehiculo : \n";
	s += coche.getDatosCarro();
	
	
	return s;
    }
    
}

